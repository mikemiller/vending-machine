class Stock
  attr_accessor :products

  def initialize(stock)
    @products = stock
  end

  def list
    products.each do |item|
      if item[:item_quantity] > 0
        Interface.stock_level(item)
      else
        Interface.unavailable_item(item[:item_name])
      end
    end
  end

  def available?(item_name)
    quantity = lookup_item(item_name)&.dig(:item_quantity)
    return false unless quantity
    quantity.positive?
  end

  def remove(item_name)
    product = lookup_item(item_name)
    return unless product
    raise UnavailableItemError if product[:item_quantity] <= 0
    product[:item_quantity] -= 1
  end

  def lookup_item(item_name)
    products.find { |product| product[:item_name] == item_name }
  end

  class UnavailableItemError < StandardError
  end
end
