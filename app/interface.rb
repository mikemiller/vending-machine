class Interface
  def self.choose_item
    puts 'What would you like to buy?'
    gets.chomp
  end

  def self.request_change(remainder_to_pay)
    puts "You have #{remainder_to_pay} pence left to pay"
    puts 'Please enter a coin (1p, 2p, 5p, 10p, 20p, 50p, £1, £2) or exit'
    gets.chomp
  end

  def self.welcome_message
    puts 'Hello! Here is our stock list:'
  end

  def self.unavailable_item(item_name)
    puts "#{item_name} is out of stock."
  end

  def self.stock_level(item)
    puts "#{item[:item_name]} - #{item[:item_quantity]} available."
  end

  def self.return_change(value)
    puts "Please take your change: #{value}"
  end

  def self.success_message
    puts 'Thanks. Please take your item.'
  end

  def self.invalid_coin
    puts 'Invalid coin. Please input one of the coins listed exactly as shown'
  end

  def self.exit_message
    puts 'Have a great day!'
  end
end
