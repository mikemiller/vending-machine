class Change
  attr_accessor :available_change

  def initialize(initial_change)
    @available_change = initial_change
  end

  def add(value)
    coin = lookup_change(value)
    return unless coin
    coin[:quantity] += 1
  end

  def lookup_change(coin_value)
    available_change.find { |coin| coin[:coin_value] == coin_value }
  end
end
