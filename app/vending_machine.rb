require_relative 'change_calculator'
require_relative 'stock'
require_relative 'interface'

class VendingMachine
  attr_reader :stock, :initial_change
  DRINKS = [{ item_name: 'fanta', item_value: 100, item_quantity: 10 },
            { item_name: 'cola', item_value: 100, item_quantity: 10 },
            { item_name: 'oj', item_value: 80, item_quantity: 10 }].freeze
  CHANGE = [{ coin_name: '5p', coin_value: 5, quantity: 10 },
            { coin_name: '10p', coin_value: 10, quantity: 10 },
            { coin_name: '20p', coin_value: 20, quantity: 10 },
            { coin_name: '50p', coin_value: 50, quantity: 10 },
            { coin_name: '£1', coin_value: 100,  quantity: 10 },
            { coin_name: '£2', coin_value: 200,  quantity: 10 }].freeze

  def initialize
    @stock = Stock.new(DRINKS)
    @initial_change = Change.new(CHANGE)
  end

  def start
    welcome_message
    stock.list
    item_name = choose_item
    if available?(item_name)
      total_value = return_cost(item_name)
      result = ChangeCalculator.new(initial_change, total_value).call
      remove_item_from_stock(item_name) if result
    else
      unavailable_item(item_name)
    end
  end

  private

  def available?(item_name)
    stock.available?(item_name)
  end

  def remove_item_from_stock(item)
    stock.remove(item)
  end

  def choose_item
    Interface.choose_item
  end

  def unavailable_item(item_name)
    Interface.unavailable_item(item_name)
  end

  def welcome_message
    Interface.welcome_message
  end

  def return_cost(item_name)
    stock.lookup_item(item_name).dig(:item_value)
  end
end
