require_relative 'interface'
require_relative 'change'
require_relative '../helpers/change_helper'

class ChangeCalculator
  include ChangeHelper

  def initialize(change, item_value)
    @change = change
    @item_value = item_value
    @value_entered = 0
    @customer_change = 0
  end

  def call # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
    loop do
      remainder = @item_value - @value_entered
      break if remainder <= 0
      input = request_change(remainder)
      return exit_response if input == EXIT
      unless valid?(input)
        invalid_coin_response(input)
        next
      end
      coin_value = COIN_VALUES[input]
      @change.add(coin_value)
      sum_total_value(coin_value)
      if @value_entered > @item_value
        @customer_change += @value_entered - @item_value
      end
    end
    success_message
    return_change(@customer_change) if @customer_change.positive?
    true
  end

  private

  EXIT = 'exit'.freeze

  def exit_response
    return_change(@customer_change) if @customer_change.positive?
    Interface.exit_message
    false
  end

  def request_change(remainder)
    Interface.request_change(remainder)
  end

  def invalid_coin_response(coin)
    Interface.invalid_coin
    return_change(coin)
  end

  def valid?(coin)
    true unless COIN_VALUES[coin].nil?
  end

  def sum_total_value(coin)
    @value_entered += coin
  end

  def success_message
    Interface.success_message
  end

  def return_change(coin)
    return unless coin
    Interface.return_change(coin)
  end
end
