require_relative '../app/change_calculator'
require_relative '../app/change'

RSpec.describe ChangeCalculator do
  let(:total_value) { 100 }
  let(:initial_change) do
    Change.new(
      [{ coin_name: '5p',  coin_value: 5, quantity: 10 },
       { coin_name: '10p', coin_value: 10, quantity: 10 },
       { coin_name: '20p', coin_value: 20, quantity: 10 },
       { coin_name: '50p', coin_value: 50, quantity: 10 },
       { coin_name: '£1', coin_value: 100,  quantity: 10 },
       { coin_name: '£2', coin_value: 200,  quantity: 10 }]
    )
  end
  let(:value_entered) { '20p' }

  subject { ChangeCalculator.new(initial_change, total_value) }
  before do
    allow(Interface).to receive(:request_change).and_return(value_entered)
  end

  describe '#call' do
    context 'small change' do
      it 'requests more change until total value is reached' do
        expect(Interface).to receive(:request_change).exactly(5).times
        expect(Interface).to receive(:success_message).once
        subject.call
      end
    end

    context 'overpayment' do
      let(:value_entered) { '£2' }
      it 'returns extra change if total value is exceeded' do
        expect(Interface).to receive(:success_message).once
        expect(Interface).to receive(:return_change).once
        subject.call
      end
    end

    context 'invalid change used' do
      let(:value_entered) { '0.3' }
      before do
        allow(Interface).to receive(:request_change).and_return(value_entered,
                                                                'exit')
      end

      it 'returns invalid coin' do
        expect(Interface).to receive(:invalid_coin).once
        expect(Interface).to receive(:return_change).with('0.3')
        subject.call
      end
    end

    context 'early exit' do
      let(:value_entered) { 'exit' }

      it 'returns extra change if total value is exceeded' do
        expect(Interface).to receive(:exit_message).once
        subject.call
      end
    end
  end
end
