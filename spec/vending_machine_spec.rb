require_relative '../app/vending_machine'

RSpec.describe VendingMachine do
  subject { VendingMachine.new }
  let(:item) { 'cola' }
  let(:item_value) { 100 }

  context 'initializes' do
    it 'initializes with Stock and Change' do
      expect(subject.stock).to be_instance_of(Stock)
      expect(subject.initial_change).to be_instance_of(Change)
    end
  end

  context 'starts' do
    before do
      calculator = double(
        ChangeCalculator.new(subject.initial_change, item_value)
      )
      allow(calculator).to receive(:call).and_return(true)
      allow(ChangeCalculator).to receive(:new).and_return(calculator)
    end

    it 'initializes the change calculator if item is available' do
      expect(Interface).to receive(:choose_item).and_return(item)
      expect(ChangeCalculator).to receive(:new).with(
        subject.initial_change,
        item_value
      )
      subject.start
    end
  end
end
