require_relative '../app/change'

RSpec.describe Change do
  subject { Change.new(initial_change) }
  let(:initial_change) do
    [{ coin_name: '5p',  coin_value: 5, quantity: 10 },
     { coin_name: '10p', coin_value: 10, quantity: 10 },
     { coin_name: '20p', coin_value: 20, quantity: 10 },
     { coin_name: '50p', coin_value: 50, quantity: 10 },
     { coin_name: '£1', coin_value: 100,  quantity: 10 },
     { coin_name: '£2', coin_value: 200,  quantity: 10 }]
  end

  describe '#new' do
    context 'no initial change' do
      let(:initial_change) { [] }
      it 'should set up change' do
        expect(subject.available_change).to be_instance_of Array
        expect(subject.available_change).to be_empty
      end
    end

    context 'with initial change' do
      it 'should set up change' do
        expect(subject.available_change).to be_instance_of Array
        coin_keys = subject.available_change.map(&:keys).uniq.flatten
        expect(coin_keys).to match_array %i[coin_name coin_value quantity]
      end
    end
  end

  describe '#add' do
    context 'change has been added' do
      let(:coin_value) { 10 }
      it 'adds coin to existing change' do
        subject.add(coin_value)
        expect(subject.lookup_change(coin_value)[:quantity]).to eq(11)
      end
    end
  end

  describe '#remove' do
    context 'change is returned' do
      let(:coin_value) { 10 }
      it 'adds coin to existing change' do
        subject.add(coin_value)
        expect(subject.lookup_change(coin_value)[:quantity]).to eq(11)
      end
    end
  end
end
