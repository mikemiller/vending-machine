require_relative '../app/stock'

RSpec.describe Stock do
  let(:products) do
    [{ code: '001', item_name: 'fanta', item_value: 100, item_quantity: 2 },
     { code: '002', item_name: 'cola', item_value: 100, item_quantity: 10 },
     { code: '003', item_name: 'oj', item_value: 80, item_quantity: 10 }].freeze
  end
  let(:item_name) { 'fanta' }
  subject { Stock.new(products) }

  it 'initialises with an array of products' do
    expect(subject.products).to be_an_instance_of(Array)
    expect(subject.products).to_not be_empty
  end

  it 'can remove an item' do
    subject.remove(item_name)
    expect(subject.lookup_item(item_name)[:item_quantity]).to eq(1)
  end

  context 'item out of stock' do
    before do
      subject.remove(item_name)
      subject.remove(item_name)
    end
    it 'returns false if item empty' do
      expect { subject.remove(item_name) }.to raise_exception(Stock::UnavailableItemError)
    end
  end
end
